class Collapsable {
  constructor(elem) {
    this.elem = document.querySelector(elem);
    this.button = this.elem.querySelector('.button--collapsable');
    this.buttonText = this.button.querySelector('span');
    this.isOpen = false;
    this.openText = 'View more';
    this.closeText = 'View less';
  }

  toggle() {
    if (this.isOpen) {
      this.elem.classList.remove('is-open');
      this.buttonText.innerText = this.openText;
    } else {
      this.elem.classList.add('is-open');
      this.buttonText.innerText = this.closeText;
    }

    this.isOpen = !this.isOpen;
  }

  init() {
    this.button.addEventListener('click', () => {
      this.toggle();
    });
  }
}

if (document.querySelector('.js-collapsable')) {
  const collapsable = new Collapsable('.js-collapsable');
  collapsable.init();
}
