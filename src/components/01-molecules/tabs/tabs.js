class Tabs {
  constructor(elem) {
    this.elem = document.querySelector(elem);
    this.navButton = this.elem.querySelectorAll('.tabs__button');
    this.navButtonLength = this.navButton.length;
    this.contentTabs = this.elem.querySelectorAll('.tabs__content');
  }

  hideContentTabs() {
    this.contentTabs.forEach(contentTab => {
      contentTab.classList.remove('is-active');
    });
  }

  deactivateTabs() {
    this.navButton.forEach(tab => {
      tab.classList.remove('is-active');
    });
  }

  activateTab(activeElement) {
    const target = activeElement.getAttribute('data-target');
    this.deactivateTabs();
    this.hideContentTabs();
    activeElement.classList.add('is-active');
    this.elem.querySelector(`#${target}`).classList.add('is-active');
  }

  attachEventListeners() {
    this.navButton.forEach(button => {
      button.addEventListener('click', e => {
        this.activateTab(e.target);
      });
    });
  }

  init() {
    this.attachEventListeners();
  }
}

if (document.querySelector('.js-tabs')) {
  const tabs = new Tabs('.js-tabs');
  tabs.init();
}
