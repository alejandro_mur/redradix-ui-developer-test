if (document.querySelector('.rating')) {
  const rating = new CountUp('animateRating', 0, 8.7, 1);
  rating.start();

  const progressBar = document.querySelector('.rating__progress');
  const progressBarValue = progressBar.getAttribute('data-progress');
  setTimeout(() => {
    progressBar.style.setProperty('--progress-bar-width', progressBarValue);
  }, 500);
}
