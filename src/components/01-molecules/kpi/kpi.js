if (document.querySelector('.kpi')) {
  const kpis = document.querySelectorAll('.kpi');
  kpis.forEach((k, idx) => {
    let kpiIdValue = k.getAttribute('data-kpi');
    let kpiText = k.querySelector('.kpi__value strong');
    let kpi = new CountUp(kpiText, 0, kpiIdValue, 0, (idx + 1) * 1.9);
    kpi.start();
  });
}
