class TogglerMenu {
  constructor(elem) {
    this.elem = document.querySelector(elem);
    this.openButton = this.elem.querySelector('.js-open-navigation');
    this.closeButton = this.elem.querySelector('.js-close-navigation');
    this.isOpen = false;
  }

  toggle() {
    if (this.isOpen) {
      this.elem.classList.remove('is-open');
    } else {
      this.elem.classList.add('is-open');
    }

    this.isOpen = !this.isOpen;
  }

  init() {
    this.openButton.addEventListener('click', () => {
      this.toggle();
    });
    this.closeButton.addEventListener('click', () => {
      this.toggle();
    });
  }
}

if (document.querySelector('.navigation')) {
  const toggleNavigation = new TogglerMenu('.navigation');
  toggleNavigation.init();
}
