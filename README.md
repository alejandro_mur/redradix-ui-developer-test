# Redradix UI Developer Test

Aplicación generada con [Fractal](http://fractal.build) como herramienta para crear la librería de componentes que nos servirán a su vez para crear templates, mantener y evolucionar la aplicación.

## Desarrollo

Tanto si vamos realizar modificaciones en los estáticos **HTML, CSS, JS o IMG** como si vamos a crear nuevos componentes.

- Para instalar las dependencias del proyecto ejecutar `npm install`
- Para empezar a desarrollar ejecutar `gulp`
- Para generar la versión para producción ejecutar `gulp build`

### Estructura

```
|-- build/
|-- src/
    |-- assets/
    |-- components/
    |-- docs/
```

### A tener en cuenta

Para animar los contadores utilizamos **countup.js [versión 1.9.3](https://github.com/inorganik/countUp.js/releases/tag/v1.9.3)**. Hay que mover la librería a `src/assets/js/vendor/`

## Recursos

- Enlace al [InVision](https://invis.io/AQU9YG6DS3V)
- Fuente Opens sans [Descargar](https://fonts.google.com/specimen/Open+Sans)
